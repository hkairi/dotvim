filetype off

set runtimepath+=~/.vim/bundle/vundle

call vundle#rc()

Bundle "https://github.com/gmarik/vundle"

" System:
Bundle "https://github.com/clones/vim-l9.git"

" Gundo
Plugin 'https://github.com/sjl/gundo.vim'
" Ultimate auto-completion system for Vim
Bundle "https://github.com/ervandew/supertab.git"

" Show marks in the gutter for various items (marks, quickfix, location, errors)
Bundle "https://github.com/tomtom/quickfixsigns_vim.git"

" Pairs of Mappings in Vim
Bundle "https://github.com/tpope/vim-unimpaired.git"

" Vim plugin that displays tags in a window, ordered by class etc
Bundle "https://github.com/majutsushi/tagbar.git"

" Fuzzy file, buffer, mru and tag finder
Bundle "https://github.com/ctrlpvim/ctrlp.vim"

" Support text objects bound to indention level
Bundle "https://github.com/austintaylor/vim-indentobject.git"

" Vim plugin, provides insert mode auto-completion for quotes, parens, brackets, etc.
Bundle "https://github.com/Townk/vim-autoclose.git"

" quoting/parenthesizing made simple
Bundle "https://github.com/tpope/vim-surround.git"

" a Git wrapper so awesome, it should be illegal
Bundle "https://github.com/tpope/vim-fugitive.git"

" Programming:
" General:
" Syntax checking hacks for vim
Bundle "https://github.com/scrooloose/syntastic.git"
" Plugin 'Quramy/tsuquyomi'

" GoLang
Plugin 'fatih/vim-go'
Plugin 'sebdah/vim-delve'

" Javascript:
" Vastly improved vim's javascript indentation.
Bundle "https://github.com/pangloss/vim-javascript.git"
Plugin 'isRuslan/vim-es6'

" JSON Highlighting for Vim
Bundle "https://github.com/leshill/vim-json.git"

" MatchParen for HTML tags
Bundle "https://github.com/gregsexton/MatchTag.git"

" NERDTree
Bundle 'https://github.com/scrooloose/nerdtree.git'

" delimitMate
Bundle 'https://github.com/Raimondi/delimitMate.git'

Plugin 'https://github.com/sheerun/vim-polyglot'

" Emmet for HTML and CSS completion
Plugin 'mattn/emmet-vim'

" HTML5
Bundle 'othree/html5.vim'

" The engine.
Plugin 'honza/vim-snippets'

" SnipMate and its dependencies:
Plugin 'MarcWeber/vim-addon-mw-utils'
Plugin 'tomtom/tlib_vim'
Plugin 'honza/vim-snipmate'

" VIM Rails
Bundle 'tpope/vim-rails'

Plugin 'tpope/vim-dadbod'
" Rspec
Plugin 'thoughtbot/vim-rspec'
" EasyDir
Plugin 'duggiefresh/vim-easydir'

" searching tool
Plugin 'https://github.com/rking/ag.vim'

" Rubocop
Plugin 'https://github.com/ngmy/vim-rubocop'

" Extras
Bundle 'matze/vim-move'
Plugin 'tommcdo/vim-exchange'

" statusline
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'

" Tabularize
Plugin 'godlygeek/tabular'

" Narrowing
" Plugin 'chrisbra/NrrwRgn'

" Rust
Plugin 'rust-lang/rust.vim'
Plugin 'racer-rust/vim-racer'

" TOML
Plugin 'cespare/vim-toml'

Plugin 'yuqio/vim-darkspace'
Plugin 'sainnhe/sonokai'

" Goyo : distraction-free Vim
Plugin 'junegunn/goyo.vim'

" HashiCorp tools
Plugin 'hashivim/vim-hashicorp-tools'

" Vim Shell Executor
Plugin 'https://github.com/JarrodCTaylor/vim-shell-executor'
Plugin 'artur-shaik/vim-javacomplete2'

" Cypher language
Plugin 'neo4j-contrib/cypher-vim-syntax'
