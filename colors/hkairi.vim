set background=light

hi clear

if exists("syntax_on")
  syntax reset
endif

" Set environment to 256 colours
set t_Co=256

let colors_name = "sweyla354554"

if version >= 700
  hi CursorLine     guibg=#FEFFFD ctermbg=231
  hi CursorColumn   guibg=#FEFFFD ctermbg=231
  hi MatchParen     guifg=#1A4700 guibg=#FEFFFD gui=bold ctermfg=22 ctermbg=231 cterm=bold
  hi Pmenu          guifg=#000000 guibg=#C8C8C8 ctermfg=16 ctermbg=251
  hi PmenuSel       guifg=#000000 guibg=#3A7D80 ctermfg=16 ctermbg=66
endif

" Background and menu colors
hi Cursor           guifg=#000000 guibg=#000000 ctermbg=16 gui=none
hi Normal           guifg=#000000 guibg=#FEFFFD gui=none ctermfg=16 ctermbg=231 cterm=none
hi NonText          guifg=#000000 guibg=#EFF0EE gui=none ctermfg=16 ctermbg=255 cterm=none
hi LineNr           guifg=#B7B8B6 guibg=#E5E6E4 gui=none ctermfg=249 ctermbg=254 cterm=none
hi StatusLine       guifg=#000000 guibg=#D6E5E4 gui=italic ctermfg=16 ctermbg=254 cterm=italic
hi StatusLineNC     guifg=#000000 guibg=#D6D7D5 gui=none ctermfg=16 ctermbg=188 cterm=none
hi VertSplit        guifg=#000000 guibg=#E5E6E4 gui=none ctermfg=16 ctermbg=254 cterm=none
hi Folded           guifg=#000000 guibg=#FEFFFD gui=none ctermfg=16 ctermbg=231 cterm=none
hi Title            guifg=#3A7D80 guibg=NONE	  gui=bold ctermfg=66 ctermbg=NONE cterm=bold
hi Visual           guifg=#014E80 guibg=#C8C8C8 gui=none ctermfg=24 ctermbg=251 cterm=none
hi SpecialKey       guifg=#543F03 guibg=#EFF0EE gui=none ctermfg=58 ctermbg=255 cterm=none
hi DiffChange       guibg=#FEFFB1 gui=none ctermbg=229 cterm=none
hi DiffAdd          guibg=#D7D8FD gui=none ctermbg=189 cterm=none
hi DiffText         guibg=#FECBFD gui=none ctermbg=225 cterm=none
hi DiffDelete       guibg=#FEBFBD gui=none ctermbg=217 cterm=none


" Syntax highlighting
hi Comment            guifg=#3A7D80 gui=none ctermfg=66 cterm=none
hi Constant           guifg=#543F03 gui=none ctermfg=58 cterm=none
hi Number             guifg=#543F03 gui=none ctermfg=58 cterm=none
hi Identifier         guifg=#113744 gui=none ctermfg=237 cterm=none
hi Statement          guifg=#1A4700 gui=none ctermfg=22 cterm=none
hi Function           guifg=#D8516E gui=none ctermfg=167 cterm=none
hi Special            guifg=#6C8079 gui=none ctermfg=243 cterm=none
hi PreProc            guifg=#6C8079 gui=none ctermfg=243 cterm=none
hi Keyword            guifg=#1A4700 gui=none ctermfg=22 cterm=none
hi String             guifg=#014E80 gui=none ctermfg=24 cterm=none
hi Type guifg=#0028B0 gui=none               ctermfg=19 cterm=none
hi pythonBuiltin      guifg=#113744 gui=none ctermfg=237 cterm=none
hi TabLineFill        guifg=#98B8CB gui=none ctermfg=110 cterm=none

" Font family and size
set guifont=Monospace:h27
