" Map Leader
let mapleader = ","

" Move To The First Non Whitespace Character Then To The First Column:
nmap <silent> 0 :call g:PersonalVim_ToggleHomeKey()<CR>

" Move To The Last Non Whitespace Character Then To The First Column:
nmap <silent> $ :call g:PersonalVim_ToggleEndKey()<CR>

" Yank From The Cursor To The End Of The Line:
nnoremap Y y$

" Yanking In Visual Mode Leaves Cursor At The Same Location:
vnoremap y ygv<esc>

" Always Use Paste Mode When Putting A Buffer:
nnoremap <silent> p :set paste<CR>p:set nopaste<CR>
nnoremap <silent> P :set paste<CR>P:set nopaste<CR>
set pastetoggle=<F3>

" Toggle Search Highlighting:
nnoremap <silent> // :silent :nohlsearch<CR>

" Copy Current Path And Filename To Clipboard:
nnoremap <silent> <leader>cp :let @* = expand("%:p")<CR>
nnoremap <silent> <leader>CP :let @* = expand("%:p:h") . "/"<CR>

" Open Syntastics Error List:
nnoremap <silent> <leader>e :Errors<CR>

" Close Syntastics Error List If Below:
nnoremap <silent> <leader>E <C-W>j :bd<CR>

" Move Around Via Methods:
nmap <silent> } }zzzv
nmap <silent> { {zzzv

" Mimic Unimpaired Style For Navigating Tabs:
noremap <silent>[r :tabprevious<CR>
noremap <silent>]r :tabnext<CR>
noremap <silent>[R <C-O>:tabfirst<CR>
noremap <silent>]R <C-O>:tablast<CR>

" Mimic Unimpaired Style For Navigating Cursor History:
nnoremap <silent>[i <C-O>
nnoremap <silent>]i <C-I>

" Tags:
" Jump To Tag In A Vertical Split:
nnoremap <silent> gr :let word=expand("<cword>")<CR>:vsp<CR>:wincmd w<cr>:exec("tag ". word)<cr>

" Show The Syntax Highlight Names Under The Cursor:
nnoremap <F10> :echo "hi<" . synIDattr(synID(line("."),col("."),1),"name") . '> trans<' . synIDattr(synID(line("."),col("."),0),"name") . "> lo<" . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">"<CR>

" Create Window Splits Easier:
nnoremap <silent> tt :tabnew<CR>
nnoremap <silent> tn :tabNext<CR>
nnoremap <silent> td mA:tabe<space>%<CR>gT`Agt`A
nnoremap <silent> vv <C-w>v
nnoremap <silent> ss <C-w>s

" Disable Arrow Keys For Criminies:
"inoremap <Up> <NOP>
"inoremap <Down> <NOP>
"inoremap <Left> <NOP>
"inoremap <Right> <NOP>
"noremap <Up> <NOP>
"noremap <Down> <NOP>
"noremap <Left> <NOP>
"noremap <Right> <NOP>

nmap <C-h> <C-w>h
nmap <C-j> <C-w>j
nmap <C-k> <C-w>k
nmap <C-l> <C-w>l

nmap <M-l> :resize +2<CR>
"
map <S-n> :NERDTreeToggle<CR>
map <S-t> :TagbarToggle<CR>
inoremap <tab> <c-r>=AutoCompleteTab<cr>
inoremap ;cl console.log("");<left><left><left>
nnoremap Q 0yt=A<C-r>=<C-r>"<CR><Esc>

nnoremap ' `
" Moving line
nnoremap <C-k> :m .-2<CR>==
nnoremap <C-j> :m .+1<CR>==

" Moving Selected Block
xnoremap K :move '<-2<CR>gv-gv
xnoremap J :move '>+1<CR>gv-gv

" Navigating between buffers
nnoremap <TAB> :bnext <CR>
nnoremap <S-TAB> :bprevious <CR>
nnoremap <Tab> gt
nnoremap <S-Tab> gT

" Run in terminal
nnoremap ,z :terminal %<CR>

" ctags mappings
nnoremap <leader>m <C-]>
nnoremap <leader>k <C->

" Refresh Vim In Various Ways:
nnoremap <silent> <leader>r :call g:PersonalVim_Refresh()<CR>

" Google For The Word Under The Curson:
nnoremap gl :set operatorfunc=g:PersonalVim_GoogleOperator<CR>g@
vnoremap gl :<C-u>call g:PersonalVim_GoogleOperator(visualmode())<CR>
