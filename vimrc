scriptencoding utf-8
set encoding=utf-8
set nocompatible
filetype on

set rtp+=~/.vim/bundle/Vundle.vim
"call vundle#begin()

" setting font and font-size
let g:ackprg = 'ag --nogroup --nocolor --column'
let g:ctrlp_match_window = 'bottom,order:btt,min:1,max:10,results:10'

source ~/.vim/bundles.vim
source ~/.vim/functions.vim
source ~/.vim/keybindings.vim

for f in split(glob('~/.vim/settings/*.vim'), '\n')
  exe 'source' f
endfor

source ~/.vim/global.vim
source ~/.vim/auto_commands.vim
source ~/.vim/syntax.vim

function! s:showMarks()
  marks abcdefghijklmnopqrstuvwxyz.
  echo 'Jump to a mark: '
  let choix = nr2char( getchar() )
  redraw
  execute "normal! '" . choix
endfunction

function! AutoCompleteTab()
  let col = col(".") - 1
  if !col || getline(".")[col - 1] !~ '\k'
    return "\<tab>"
  else
    return "\<C-n>"
  endif
endfunction

set runtimepath^=~/.vim/bundle/ctrlp.vim
set clipboard=unnamed
set guioptions-=m  "remove menu bar
set guioptions-=  "remove toolbar

" scrolling
set ttyfast
"set ttyscroll=3
set lazyredraw

set showmatch
set showcmd

set hidden                        " Handle multiple buffers better.

set wildmenu                      " Enhanced command line completion.
set wildmode=list:longest         " Complete files like a shell.

set ignorecase                    " Case-insensitive searching.
set smartcase                     " But case-sensitive if expression contains a capital letter.

set relativenumber                " Show line numbers.
set number
set ruler                         " Show cursor position.
set cursorline                    " Highlight current line
set incsearch                     " Highlight matches as you type.
set hlsearch                      " Highlight matches.

set wrap                          " Turn on line wrapping.
set scrolloff=3                   " Show 3 lines of context around the cursor.

set title                         " Set the terminal's title

set visualbell                    " No beeping.

set nobackup                      " Don' make a backup before overwriting a file.
set nowritebackup                 " And again.
set directory=$HOME/.vim/tmp//,.  " Keep swap files in one location

set termwinsize=10x0

" UNCOMMENT TO USE
set tabstop=2                    " Global tab width.
set shiftwidth=2                 " And again, related.
set expandtab                    " Use spaces instead of tabs
" Tab mappings.
map <leader>to :tabonly<cr>
map <leader>tn :tabnext<cr>
map <leader>tp :tabprevious<cr>
map <leader>tf :tabfirst<cr>
map <leader>tl :tablast<cr>
map <leader>tm :tabmove
map <CR> :nohl<cr>
map <leader>a :! git add %<cr>
map <leader>c :Gcommit<cr>
map <leader>p :! git push<cr>

map <leader>n :terminal<cr>

" RSpec.vim mappings
map <Leader>s :call RunCurrentSpecFile()<CR>
map <Leader>l :call RunLastSpec()<CR>

" Replacing a word in all buffers
map <leader>r :call Replace()<CR>
" Rubocop conf file
let g:vimrubocop_config = '.rubocop.yml'
let g:vimrubocop_keymap = 0

" Elm
let g:elm_format_autosave = 1
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:elm_syntastic_show_warnings = 1

let g:jsx_ext_required = 0 " Allow JSX in normal JS files
let g:syntastic_javascript_checkers = ['jsxhint']
let g:syntastic_javascript_jsxhint_exec = 'jsx-jshint-wrapper'

let g:airline_powerline_fonts = 1
let g:airline_mode_map = { '__' : '-', 'n' : 'N', 'i' : 'I', 'R' : 'R', 'c' : 'C', 'v' : 'V', 'V' : 'V', '^V' : 'V', 's' : 'S', 'S' : 'S', '^S' : 'S' }
" Trigger configuration. Do not use <tab> if you use https://github.com/Valloric/YouCompleteMe.
let g:UltiSnipsExpandTrigger="<tab>"
" Defaulting GoImports on save
let g:go_fmt_command = "goimports"

" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"
let g:typescript_compiler_binary = 'tsc'

let g:syntastic_go_checkers = ['go', 'golint', 'govet']
let g:go_def_mode='gopls'
let g:go_info_mode='gopls'

autocmd FileType typescript :set makeprg=tsc
autocmd QuickFixCmdPost [^l]* nested cwindow
autocmd QuickFixCmdPost    l* nested lwindow

autocmd FileType javascript nnoremap <Leader>k mjggvGJ<Esc>`j
autocmd FileType javascript map <leader>e :!clear <CR> :! node %<CR>
autocmd FileType ruby       map <leader>e :!clear <CR> :! ruby %<CR>
autocmd FileType go         map <leader>e :!clear <CR> :! go run %<CR>

" Testing
autocmd FileType elixir     map <leader> :!clear <CR> :! mix test <CR>

" format all files
" autocmd BufWritePre,BufRead *.html :normal gg=G


" abbreviations
iabbrev calss class
" enable mouse support
set mouse=a
